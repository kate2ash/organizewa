//
//  CalendarEvents.swift
//  Organizer
//
//  Created by Katok on 27.02.17.
//  Copyright © 2017 Katok. All rights reserved.
//
import EventKit
import UIKit
//class AddEventViewController: UIViewController, UIApplicationDelegate {
//
//    var calendar: EKCalendar! // Intended to be set in parent controller's prepareForSegue event
//    
//    let eventStore = EKEventStore()
//    //var delegate: EventAddedDelegate?
//
//    
//    @IBOutlet var clientsNameTextField: UITextField!
//    @IBOutlet var eventDatePicker: UIDatePicker!
//    @IBOutlet var addAndSendRequestForEvent: UIButton!
//    
//
//    
//        override func viewDidLoad() {
//            super.viewDidLoad()
//            // Do any additional setup after loading the view, typically from a nib.
//            self.eventDatePicker.setDate(initialDatePickerValue(), animated: false)
//        }
//        
//        func initialDatePickerValue() -> Date {
//            let calendarUnitFlags: NSCalendar.Unit = [.year, .month, .day, .hour, .minute, .second]
//            
//            var dateComponents = (Calendar.current as NSCalendar).components(calendarUnitFlags, from: Date())
//            
//            dateComponents.hour = 0
//            dateComponents.minute = 0
//            dateComponents.second = 0
//            
//            return Calendar.current.date(from: dateComponents)!
//        }
//        
//        @IBAction func cancelButtonTapped(_ sender: UIButton) {
//            self.dismiss(animated: true, completion: nil)
//        }
//        
//    @IBAction func addEventButtonTapped(_ sender: UIButton) {
//            // Create an Event Store instance
//            let eventStore = EKEventStore();
//            
//            // Use Event Store to create a new calendar instance
//            if let calendarForEvent = eventStore.calendar(withIdentifier: self.calendar.calendarIdentifier)
//            {
//                let newEvent = EKEvent(eventStore: eventStore)
//                
//                newEvent.calendar = calendarForEvent
//                newEvent.title = self.clientsNameTextField.text ?? "Some Event Name"
//                newEvent.startDate = self.eventDatePicker.date
//              
//                
//                // Save the event using the Event Store instance
//                do {
//                    try eventStore.save(newEvent, span: .thisEvent, commit: true)
//           //         delegate?.eventDidAdd()
//                    
//                    self.dismiss(animated: true, completion: nil)
//                } catch {
//                    let alert = UIAlertController(title: "Event could not save", message: (error as NSError).localizedDescription, preferredStyle: .alert)
//                    let OKAction = UIAlertAction(title: "OK", style: .default, handler: nil)
//                    alert.addAction(OKAction)
//                    
//                    self.present(alert, animated: true, completion: nil)
//                }
//            }
//}
//}
