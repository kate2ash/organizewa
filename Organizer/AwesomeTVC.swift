//
//  ViewController.swift
//  Organizer
//
//  Created by Katok on 25.02.17.
//  Copyright © 2017 Katok. All rights reserved.
//

import UIKit
import EventKit
class AwesomeTVC: UITableViewController {
        
        let weekList: [String] = ["Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"]
    
     

        override func viewDidLoad() {
            super.viewDidLoad()
            
            // Uncomment the following line to preserve selection between presentations
            // self.clearsSelectionOnViewWillAppear = false
            
            // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
            // self.navigationItem.rightBarButtonItem = self.editButtonItem()
        }
        
        
        // MARK: - Table view data sourc
        
        override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            // #warning Incomplete implementation, return the number of rows
            return weekList.count
        }
        
        override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
            cell.textLabel?.text = "\(indexPath.row.description)\t|\t\(weekList[indexPath.row])"
            return cell
        }
    
        override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        // Segue to the second view controller
        self.performSegue(withIdentifier: "test", sender: self)
        }
    
    // passing data from AwesomeTVC to DayTimeVC
        override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let dayTimeVC = segue.destination as! DayTimeVC
        
        // set a variable in the second view controller with the data to pass
        dayTimeVC.receivedData = "hello"
    }
    
        

    


}

