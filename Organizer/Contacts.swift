//
//  ViewController.swift
//  Organizer
//
//  Created by Katok on 25.02.17.
//  Copyright © 2017 Katok. All rights reserved.
//

import UIKit
import AddressBook
class Contacts: NSObject {
    
    
    var firstName: String!
    var lastName: String!
    var phoneNumber: String!
    var imageData: Data!
    
   override var description : String {
        return firstName + " " + lastName
    }
    
    init(firstName: String, lastName: String, phoneNumber: String, imageName: String) {
        self.firstName = firstName
        self.lastName = lastName
        self.phoneNumber = phoneNumber
        self.imageData = UIImageJPEGRepresentation(UIImage(named: imageName)!, 0.7)
    }
    

    //image for person
//    func imageForPerson(person: ABRecord) -> UIImage?{
//        let data = ABPersonCopyImageData(person).takeRetainedValue() as NSData
//        let image = UIImage(data: as data)
//        return image
    
}
