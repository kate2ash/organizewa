//
//  DayTimeVC.swift
//  Organizer
//
//  Created by Katok on 26.02.17.
//  Copyright © 2017 Katok. All rights reserved.
//

import UIKit

class DayTimeVC: UITableViewController {
    
    
    @IBAction func addNewEvent(_ sender: UIBarButtonItem) {
   
    }
    let day: [String] = ["07:00", "07:30", "08:00", "08:30", "09:00", "09:30", "10:00", "10:30", "11:00", "11:30", "12:00", "12:30", "13:00", "13:30", "14:00", "14:30", "15:00", "15:30", "16:00", "16:30", "17:00", "17:30", "18:00", "18:30", "19:00", "19:30", "20:00", "20:30", "21:00", "21:30", "22:00", "22:30", "23:00", "23:30"]
    
    var receivedData = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false
        
        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
     print(receivedData)
        
    }
    
    
    // day sheduled
  override  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return day.count
    }
    
    
    
    
   override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellDay = tableView.dequeueReusableCell(withIdentifier: "cellDay", for: indexPath)
        cellDay.textLabel?.text = "\(day[indexPath.row])"
        return cellDay
        
    }
}

